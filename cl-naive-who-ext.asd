(defsystem "cl-naive-who-ext"
  :description "This is a simple extension to use with the venerable cl-who. It introduces pre-processing to enable the creation of custom html tags for example (:my-tag ...)."
  :version "2021.5.7"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-getx :cl-who)
  :components ((:file "src/package")
	       (:file "src/common" :depends-on ("src/package"))
	       (:file "src/utils" :depends-on ("src/common"))
	       (:file "src/who-ext" :depends-on ("src/utils"))
	       (:file "src/helpers" :depends-on ("src/who-ext"))))

