(defsystem "cl-naive-who-ext.inject"
  :description "This is a simple extension of the venerable cl-who. It adds pre-processing to cl-who to enable the creation of custom html tags for example (:my-tag :custom-attribute = \"some value\"). It clobbers with-html-output to inject the pre-processing."
  :version "2021.5.7"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-getx :cl-who)
  :components ((:file "src/inject/package")
	       (:file "src/common" :depends-on ("src/inject/package"))
	       (:file "src/utils" :depends-on ("src/common"))
	       (:file "src/who-ext" :depends-on ("src/utils"))
	       (:file "src/inject/inject" :depends-on ("src/who-ext"))
	       (:file "src/inject/helpers" :depends-on ("src/inject/inject"))))

