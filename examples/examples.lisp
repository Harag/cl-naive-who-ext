(in-package :common-lisp-user)

(defpackage :cl-naive-who-ext.examples
  (:use :cl :cl-naive-who-ext)
  (:export))

(in-package :cl-naive-who-ext.examples)

;;;;The basics

(define-tag (:bh)
  ;;Body mostly looks like this
  `(:h1 :shoe-size
        ;;get attribute shoe-size
        ,(att :shoe-size atts)
        ;;parse other attributes other than :shoe-size
        ,@(atts '(:shoe-size) atts)
        ;;expand the body
        ,@body))

(with-html-to-string
  (:div ;;works with normal html tags
   (:bh ;;our custom tag
    :shoe-size 5 ;;custom attr
    :pants-size 34 ;;custom attr
    "Shoe!")))

(let ((size 2)
      (text "Yeahaaaa!"))
  (with-html-to-string
    (:div (:bh :shoe-size size (cl-who:esc text)))))

(with-html-to-string
  (let ((size 2)
        (text "Yeahaaaa XXX!"))
    (cl-who:htm
     (:div (:bh :shoe-size size (cl-who:esc text))))))

(with-html-to-string
  (let ((size 2)
        (text "Yeahaaaa XXX!"))
    (cl-who:htm
     (:div (:bh :shoe-size size (:span (cl-who:esc text)))))))

;;;;Using variables

#|
"
<div>
<h1 shoe-size='5' pants-size='34'>Shoe!
</h1>
</div>"
|#

;;;; Simple Examples

(define-tag (:simple)
  `(:span :eish (if (= 1 (random 2))
                    "random-value"
                    "other-value")
          "Yeeeha Granma!!"))

(with-html-to-string
  (:simple))

(define-tag (:container)
  `(:div :style "width:500px;"
         ,@(atts)
         ,@body))

(with-html-to-string
  (:container :id "eish"
              (:span "Eish me!")
              (:span "Me eish!")))

(define-tag (:card)
  `(:container
    (:h1 ,(att :heading atts))
    (:div :style ,(format nil "background-color:~A;"
                          (att :bg-color atts))
          ;;Not allowing custom tags in the body so any passed will not
          ;;be pre-processes.
          ,@body)))

;; Standard tags allowed
(with-html-to-string
  (:card :id "eish"
         :bg-color "pink"
         (:span "Eish me!")
         (:span "Me eish!")))

;; Custom tags not allowed
(with-html-to-string
  (:card :id "eish"
         (:simple)))

(define-tag (:ext-select)
  `(:select
    ,@(atts)
    ,@body))

(cl-naive-who-ext::with-html-to-string
  (:ext-select :id "select"
               (:option "Option A")
               (:option "Option B")))

(cl-naive-who-ext::with-html-to-string
  (let ((options (list "Option A"
                       "Option B")))
    (cl-who:htm
     (::ext-select :id "select"
                   (dolist (option options)
                     (cl-who:htm (:option (cl-who:str option))))))))

;;Test cl-who special ((:tag) "slurp this") syntax
(define-tag (:ext-slurp-select)
  `((:select)
    (:option "1")
    (:option "2")))

(cl-naive-who-ext::with-html-to-string
  (:container
   (:card :heading "My Container" :bg-color (if (> 5 (random 10))
                                                "light-gray"
                                                "light-green")
          (:span "Some text in a span")
          (:span "Some more text in a span")
          (:simple)))
  (:ext-slurp-select)
  (if (> 5 (random 10))
      (cl-who:htm (:simple))
      (cl-who:str "Random Madness!")))

#|
;; Attempting a tag macro, not getting it right

(defun intern% (string)
(intern (string-upcase string) :KEYWORD))

(define-tag (:rw-h)
;;(break "~S" (att :size))
``(,(eval `(intern% (format nil "h~A" (att :size))))
,,@body))

|#

