(defsystem "cl-naive-who-ext.tests"
  :description "Tests for cl-naive-who-ext."
  :version "2021.5.7"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-who-ext :cl-naive-tests :cl-who)
  :components (
	       (:file "tests/package")
	       (:file "tests/tests" :depends-on ("tests/package"))))

