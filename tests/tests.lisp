(in-package :cl-naive-who-ext.tests)

(define-tag (:simple)
  `(:span :eish "value" "Yeeeha Granma!!"))

(define-tag (:simple-dynamic)
  `(:span :eish (if (= 1 (random 2))
                    "random-value"
                    "random-value")
          "Yeeeha Granma!!"))

(define-tag (:container)
  `(:div :style "width:500px;"
     ;;Include other attributes passed to the tag like :id
     ,@(atts nil atts)
     ,@body))

(define-tag (:card)
  `(:container
    (:h1 ,(atts :heading))
    (:div :style ,(format nil "background-color:~A"
                          (att :bg-color atts))
      ;;Not allowing custom tags in the body so any passed will not
      ;;be preprocesses.
      ,@body)))

(define-tag (:ext-select)
  `(:select
     ,@(atts nil atts)
     ,@body))

(define-tag (:ext-slurp-select)
  `((:select)
    (:option "1")
    (:option "2")))

(testsuite  :cl-naive-who-ext

  (testcase :simple
            :expected "
<span eish='value'>Yeeeha Granma!!
</span>"
            :actual (with-html-to-string (:simple))
            :info "Simple tag, no attributes")

  (testcase :simple-dynamic
            :expected "
<span eish='random-value'>Yeeeha Granma!!
</span>"
            :actual (with-html-to-string (:simple-dynamic))
            :info "Simple tag, with dynamic attribute to show that pre-processor works with dynamic attributes.")

  (testcase :simple-attributes
            :expected "
<container-dynamic att='att-value'>Yeeeha Granma!
</container-dynamic>"
            :actual (with-html-to-string (:container-dynamic :att "att-value" "Yeeeha Granma!"))
            :info "Simple tag, with dynamic attribute to show that pre-processor works with dynamic attributes.")

  (testcase :tag-within-tag
            :expected "
<div style='width:500px;' att='att-value'>
<span eish='value'>Yeeeha Granma!!
</span>
</div>"
            :actual (with-html-to-string
                      (:container :att "att-value"
                                  (:simple)))
            :info "Tag within tag allowed.")

  (testcase :custom-attributes-not-allowed
            :expected "
<div style='width:500px;'>
  <h1>
  </h1>
  <div style='background-color:pink'>
<span eish='value'>Yeeeha Granma!!
</span>
  </div>
</div>"
            :actual (with-html-to-string
                      (:card :heading "Child Not Allowed"
                             :bg-color "pink"
                             (:simple)))
            :info "Only some attributes allowed.")

  (testcase :tag-without-slurp
            :expected "
<select id='select'>
  <option>Option A
  </option>
  <option>Option B
  </option>
</select>"
            :actual (with-html-to-string
                      (:ext-select :id "select"
                                   (:option "Option A")
                                   (:option "Option B")))
            :info "Select without slurps.")
  (testcase :tag-with-slurp
            :expected "
<select>
  <option>1
  </option>
  <option>2
  </option>
</select>"
            :actual (with-html-to-string
                      (:ext-slurp-select))
            :info "Select with slurp. Just checking that pre-processor does not interfere with who special syntax.")

  (testcase :kitchen-sink
            :expected "
<div style='width:500px;'>
<div style='width:500px;'>
  <h1>
  </h1>
  <div style='background-color:light-gray'>
<span eish='value'>Yeeeha Granma!!
</span>
  </div>
</div>
</div>
<select>
  <option>1
  </option>
  <option>2
  </option>
</select>
<span eish='value'>Yeeeha Granma!!
</span>"
            :actual (print (with-html-to-string
                             (:container
                              (:card :heading "My Container" :bg-color (if (> 5 (random 10))
                                                                           "light-gray"
                                                                           "light-gray")
                                     (:span "Some text in a span")
                                     (:span "Some more text in a span")
                                     (:simple)))
                             (:ext-slurp-select)
                             (if (> 5 (random 10))
                                 (cl-who:htm (:simple))
                                 (cl-who:htm (:simple)))))
            :info "Test it all together."))
;;(report (run))
