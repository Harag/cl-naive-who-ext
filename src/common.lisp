(in-package :cl-naive-who-ext)

(defun atts (&optional exl atts)
  (let ((ex-atts))
    (loop :for (a b) :on atts :by #'cddr
          :unless (member a exl :test 'equal)
          :do
          (push a ex-atts)
          (push b ex-atts))
    (reverse ex-atts)))

(defun att (key &optional atts)
  (let ((val (getf atts key)))
    (cond ((and (symbolp val) (not (boundp val)))
           val)
          ((listp val)
           (eval val))
          (t val))))
