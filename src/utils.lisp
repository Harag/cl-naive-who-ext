(in-package :cl-naive-who-ext)

(defun flatten (l)
  (cond ((null l) nil)
        ((atom l) (list l))
        (t (loop for a in l appending (flatten a)))))

(defun lts (&rest list)
  "Returns a string with list items delimited with a space.
Typically used with css class attribute."
  (format nil "~{~A~^ ~}" (remove nil (flatten list))))

